<?php namespace DPS\HtmlBuilder\Tests;

use Mockery as m;
use Illuminate\View\Compilers\BladeCompiler;
class DirectivesTest extends TestCase
{
    protected $compiler;

    public function setUp()
    {
        parent::setUp();
        $this->compiler = $this->app[BladeCompiler::class];
    }

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }


    public function testClassesDirective()
    {
        $this->assertEquals('<?php echo collect([\'foo\' => true, \'bar\' => false])->filter()->keys()->implode(\' \') ?>', $this->compiler->compileString('@class([\'foo\' => true, \'bar\' => false])'));
    }
}